﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyItem1 : MonoBehaviour
{
    [SerializeField]
    float life, score;

    float currentLife;
    void Start()
    {
        currentLife = life;
    }

    public float GetLife()
    {
        return currentLife;
    }

    public void SetLife(float capacity)
    {
        currentLife -= capacity;
    }

    public float GetScore()
    {
        return score;
    }

}
