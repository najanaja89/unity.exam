﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{

    [SerializeField]
    float maxAmmo, damage, range, shootSpeed;
    float clip;
    [SerializeField]
    float currentAmmo;
    [SerializeField]
    ParticleSystem shootEffect;
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    GameObject hitEffect;
    [SerializeField]
    Camera playerCamera;

    [SerializeField]
    Text ammoUI;
    [SerializeField]
    Text clipUI;

    [SerializeField]
    Player player;


    void Start()
    {
        clip = 10;
        currentAmmo = maxAmmo - clip;
        clipUI.text = "Clip: " + clip.ToString();
        ammoUI.text = "Ammo: " + currentAmmo.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {

            if (clip > 0)
            {
                Shoot();
                clip--;
                clipUI.text = "Clip: " + clip.ToString();

            }
            else if (currentAmmo > 0)
            {
                clip = 10;
                currentAmmo -= clip;
                ammoUI.text = "Ammo: " + currentAmmo.ToString();
            }
        }
    }

    public void PickUpAmmo(float capacity)
    {
        if (currentAmmo <= 30)
        {
            currentAmmo += capacity;
            ammoUI.text = "Ammo: " + currentAmmo.ToString();
        }
    }

    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, range))
        {
            shootEffect.Play();
            GameObject effect = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(effect, 1f * Time.deltaTime);
            Debug.Log($"Hit name {hit.collider.name}");
            var destroyItem1 = hit.transform.GetComponent<DestroyItem1>();

            if (destroyItem1.GetLife() > 0)
            {
                destroyItem1.SetLife(1);
            }
            else
            {
                player.SetScore(destroyItem1.GetScore());
                Destroy(hit.transform.gameObject);
            }

        }
    }

}
