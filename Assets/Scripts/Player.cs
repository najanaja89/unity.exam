﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    [SerializeField]
    GameObject player;
    [SerializeField]
    Gun heavyWeapon;

    [SerializeField]
    Text scoreUI;

    float score;

    [SerializeField]
    private int maxLife;
    private int curLife;


    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        curLife = maxLife;
    }
    public void SetScore(float capacity)
    {
        score += capacity;
        scoreUI.text = "Score: " + score.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ClipItem")
        {
            heavyWeapon.PickUpAmmo(10);
            Destroy(other.gameObject);
        }
    }

    public void HealthBarAdd(int capacity)
    {
        curLife = capacity;
    }

    public int CurrentHealth()
    {
        return curLife;
    }
    public void HealthBarMinus(int capacity)
    {
        curLife -= capacity;
    }
    public void SetHealth(int health)
    {
        curLife = health;
    }

}
